# BSQ

Algorithm to find the biggest square in a map with obstacles in Rust.
(Requires Perl for map generation)



## Usage

Map generation:
```
./gen.pl 100 100 100 >test
```

Compilation:
```
cargo build --release
```

Running the algorithm with the map:
```
cat test | target/release/BSQ
```
