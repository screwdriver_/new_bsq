use std::io;
use std::cmp::min;

/*
 * Takes a map under the form of a vector of strings and writes `.` at the location of the biggest
 * square.
 */
fn solve(lines: &mut Vec<String>) {
    let height = lines.len();
    let width = lines[0].len();

    if height == 0 {
        return;
    }

    /*
     * A sum matrix, used to compute the size of the square using the previous small squares.
     * The matrix is initialized with zeros.
     */
    let mut sum = Vec::<usize>::new();
    sum.resize(height * width, 0);

    /*
     * Fills the first column of the sum matrix.
     */
    for i in 0..height {
        sum[i * width] = if lines[i].as_bytes()[0] as char == '0' { 1 } else { 0 };
    }

    /*
     * Fills the first row of the sum matrix.
     */
    for j in 0..width {
        sum[j] = if lines[0].as_bytes()[j] as char == '0' { 1 } else { 0 };
    }

    /*
     * Going through every values of the map, taking the smallest value between the left, top and
     * top_left values in the sum matrix, adding one and storing it into the matrix.
     */
    for i in 1..height {
        for j in 1..width {
            if lines[i].as_bytes()[j] as char == '0' {
                let left = sum[i * width + (j - 1)];
                let up = sum[(i - 1) * width + j];
                let up_left = sum[(i - 1) * width + (j - 1)];
                sum[i * width + j] = min(left, min(up, up_left)) + 1;
            }
        }
    }

    let mut x = 0;
    let mut y = 0;
    let mut size = 0;

    /*
     * Finding the maximum value in the sum matrix, giving the position of the bottom right corner
     * of the largest square.
     */
    for i in 0..height {
        for j in 0..width {
            if size < sum[i * width + j] {
                x = j;
                y = i;
                size = sum[i * width + j];
            }
        }
    }

    /*
     * Getting the position of the top left corner.
     */
    x -= size - 1;
    y -= size - 1;

    /*
     * Writing the square on the map.
     */
    for i in y..(y + size as usize) {
        for j in x..(x + size as usize) {
            unsafe {
                lines[i].as_bytes_mut()[j] = '.' as u8;
            }
        }
    }
}

fn main() {
    let mut lines = Vec::<String>::new();

    loop {
        let mut buff = String::new();

        match io::stdin().read_line(&mut buff) {
            Ok(n) => {
                if n == 0 {
                    break;
                }
            }
            Err(_n) => {
                println!("Err");
                return;
            }
        }

        buff.truncate(buff.len() - 1);
        lines.push(buff);
    }

    solve(&mut lines);
    for j in 0..lines.len() {
        println!("{}", lines[j]);
    }
}
